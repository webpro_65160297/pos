import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt={
    id: string;
    Amount:number;
    createdDate: Date;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member:Member;
    discount:number;
    receiptItems?:ReceiptItem[]
}
export type { Receipt }