type Unit = 'กิโลกรัม' | 'ลิตร' | ''
type Material={
    id: number,
    name: string,
    QTY: number,
    unit: Unit,
    min: number
}
export {type Material}