import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { type Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    {id:1,name: 'Vernon Chwe',tel:'0656728778'},
    {id:2,name: 'Joshua Hong',tel:'0903964877'},
    {id:3,name: 'Mingyu Kim',tel:'0947954484'},
    {id:4,name: 'Manfah',tel:'0889739169'},
    {id:5,name: 'paewnosuke',tel:'0994239334'}])
    
    const currentMember = ref<Member | null>()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value= null
        }
        currentMember.value =members.value[index]
    }
    function clear(){
      currentMember.value=null
    }

  return { members,searchMember,currentMember,clear}
})