import { ref } from "vue";
import { defineStore } from "pinia";
import type { ReceiptItem } from "@/types/ReceiptItem";
import type { Product } from "@/types/Products";
import { type Receipt } from "@/types/Receipt";
import { useAuthStore } from "./auth";
import { useMemberStore } from "./member";

export const useReceiptStore = defineStore("counter", () => {
    const authStore =useAuthStore()
    const memberStore= useMemberStore()
    const receiptDialog=ref(false)
    const receipt = ref<Receipt>({
        id:'0001',
        Amount: 0,
        createdDate: new Date(),
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: "",
        userId: authStore.currentUser.id,
        user:authStore.currentUser,
        memberId: 0,
        member: {
            id: 0,
            name:'',
            tel: ""
        },
        discount: 0
    });
    const receiptItems = ref<ReceiptItem[]>([]);
    function addReceiptItem(product: Product) {
        const index = receiptItems.value.findIndex(
            (item) => item.product?.id === product.id
        );
        if (index >= 0) {
            receiptItems.value[index].unit++;
            calReceipt()
        } else {
            const newReceipt: ReceiptItem = {
                id: -1,
                name: product.name,
                price: product.price,
                unit: 1,
                productId: product.id,
                product: product,
            };

            receiptItems.value.push(newReceipt);
            calReceipt()
        }
    }
    function removeReceiptItem(receiptItem: ReceiptItem) {
        const index = receiptItems.value.findIndex((item) => item === receiptItem);
        receiptItems.value.splice(index, 1);
        calReceipt()
    }
    function inc(item: ReceiptItem) {
        item.unit++;
        calReceipt()
    }
    function dec(item: ReceiptItem) {
        if (item.unit === 1) {
            removeReceiptItem(item);
        }
        item.unit--;
        calReceipt()
    }
    function calReceipt(){
        let Amount =0
        for(const item of receiptItems.value){
            Amount=Amount+(item.price*item.unit)
        }
        receipt.value.Amount=Amount
        if(memberStore.currentMember){
            receipt.value.total=Amount*0.90
        }else{
            receipt.value.total=Amount
        }
    }
    function showReceiptDialog(){
        receiptDialog.value=true
        // show rec
        receipt.value.receiptItems=receiptItems.value
    }
    function clear(){
        receiptItems.value=[]
        receipt.value={
            id:'0001',
            Amount: 0,
            createdDate: new Date(),
            total: 0,
            receivedAmount: 0,
            change: 0,
            paymentType: "",
            userId: authStore.currentUser.id,
            user:authStore.currentUser,
            memberId: 0,
            member: {
                id: 0,
                name: "",
                tel: ""
            },
            discount: 0
        }
        memberStore.clear()
    }

    return {
        receiptItems,
        receipt,
        addReceiptItem,
        removeReceiptItem,
        inc,
        dec,calReceipt,receiptDialog,showReceiptDialog,clear
    };
});
