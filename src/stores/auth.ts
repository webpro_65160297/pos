import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
        id: 1,
        email: 'yoonzino@sev.com',
        password: '1004',
        fullName: 'Jeonghan Yoon',
        gender: 'male',
        roles: ['user']
    })

    return {currentUser}
})